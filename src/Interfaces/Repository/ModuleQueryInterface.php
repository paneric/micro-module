<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Repository;

interface ModuleQueryInterface
{
    public function adaptManager(): void;

    public function queryBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;
}
