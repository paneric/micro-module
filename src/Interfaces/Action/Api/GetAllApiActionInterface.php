<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action\Api;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllApiActionInterface
{
    public function getAll(Request $request): array;
    public function getStatus(): int;
}
