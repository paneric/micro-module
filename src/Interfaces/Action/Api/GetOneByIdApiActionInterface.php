<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action\Api;

interface GetOneByIdApiActionInterface
{
    public function getOneById(string $id): ?array;
    public function getStatus(): int;
}
