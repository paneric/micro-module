<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action\Api;

use Psr\Http\Message\ServerRequestInterface as Request;

interface AlterApiActionInterface
{
    public function update(Request $request, string $id): ?array;
    public function getStatus(): int;
}
