<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action;

interface GetAllPaginatedActionInterface
{
    public function getAllPaginated(string $page = null): array;
}
