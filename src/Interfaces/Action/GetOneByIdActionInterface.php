<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action;

interface GetOneByIdActionInterface
{
    public function getOneById(string $id): ?array;
}
