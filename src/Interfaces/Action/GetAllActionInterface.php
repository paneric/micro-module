<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action;

interface GetAllActionInterface
{
    public function getAll(): array;
}
