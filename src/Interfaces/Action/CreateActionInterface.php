<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface CreateActionInterface
{
    public function create(Request $request): ?array;
}
