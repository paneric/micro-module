<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface DeleteActionInterface
{
    public function delete(Request $request, string $id): ?array;
}
