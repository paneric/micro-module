<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Repository;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Repository;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;

class ModuleRepository extends Repository implements ModuleRepositoryInterface
{
    public function __construct(Manager $manager, array $config)
    {
        parent::__construct($manager);

        $this->table = $config['table'];
        $this->daoClass = $config['dao_class'];

        $this->createUniqueWhere = $config['create_unique_where'];
        $this->updateUniqueWhere = $config['update_unique_where'];
    }
}
