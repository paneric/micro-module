<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Controller;

use Paneric\CSRTriad\Controller\AppController;
use Paneric\MicroModule\Interfaces\Action\CreateActionInterface;
use Paneric\MicroModule\Interfaces\Action\AlterActionInterface;
use Paneric\MicroModule\Interfaces\Action\DeleteActionInterface;
use Paneric\MicroModule\Interfaces\Action\GetAllActionInterface;
use Paneric\MicroModule\Interfaces\Action\GetAllPaginatedActionInterface;
use Paneric\MicroModule\Interfaces\Action\GetOneByIdActionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ModuleController extends AppController
{
    protected $routePrefix;

    public function __construct(Twig $twig, string $routePrefix)
    {
        parent::__construct($twig);

        $this->routePrefix = $routePrefix;
    }

    public function showAll(
        Response $response,
        GetAllActionInterface $action
    ): Response{
        return $this->render(
            $response,
            '@module/show_all.html.twig',
            $action->getAll()
        );
    }

    public function showAllPaginated(
        Response $response,
        GetAllPaginatedActionInterface $action,
        string $page = null
    ): Response{
        return $this->render(
            $response,
            '@module/show_all_paginated.html.twig',
            $action->getAllPaginated($page)
        );
    }

    public function showOneById(Response $response, GetOneByIdActionInterface $action, string $id): Response
    {
        return $this->render(
            $response,
            '@module/show_one_by_id.html.twig',
            $action->getOneById($id)
        );
    }

    public function add(Request $request, Response $response, CreateActionInterface $action): Response
    {
        $result = $action->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . '/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, AlterActionInterface $action, string $id): Response
    {
        $result = $action->update($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . '/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/edit.html.twig',
            $result
        );
    }

    public function remove(Request $request, Response $response, DeleteActionInterface $action, string $id): Response
    {
        $result = $action->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/' .  $this->routePrefix . '/show-all-paginated',
                200
            );
        }

        return $this->render(
            $response,
            '@module/remove.html.twig',
            ['id' => $id]
        );
    }
}
