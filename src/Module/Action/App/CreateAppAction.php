<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\MicroModule\Interfaces\Action\CreateActionInterface;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CreateAppAction extends Service implements CreateActionInterface
{
    protected $adapter;

    protected $moduleNameSc;
    protected $createUniqueCriteria;
    protected $daoClass;
    protected $dtoClass;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->daoClass = $config['dao_class'];
        $this->dtoClass = $config['dto_class'];
        $this->createUniqueCriteria = $config['create_unique_criteria'];
        $this->moduleNameSc = $config['module_name_sc'];
    }

    public function create(Request $request): ?array
    {
        $attributes = $request->getParsedBody();

        if (
            $request->getMethod() === 'POST' &&
            empty($request->getAttribute('validation')[$this->dtoClass])
        ) {
            $dao = new $this->daoClass();
            $dao->hydrate($attributes);

            $createUniqueCriteria = $this->createUniqueCriteria;

            if ($this->adapter->createUnique($createUniqueCriteria($attributes), $dao) !== null) {
                return null;
            }

            $this->session->setFlash(['db_add_' . $this->moduleNameSc . '_unique_error'], 'error');
        }

        $this->session->setFlash(['page_title' => 'form_' . $this->moduleNameSc . '_add_title'], 'value');

        $dto = new $this->daoClass();
        $dto->hydrate($attributes ?? []);

        return [
            'dto' => $dto->convert(),
        ];
    }
}
