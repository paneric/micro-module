<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\MicroModule\Interfaces\Action\GetAllActionInterface;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetAllAppAction extends Service implements GetAllActionInterface
{
    protected $adapter;

    protected $moduleNameSc;
    protected $orderBy;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->orderBy = $config['order_by'];
        $this->moduleNameSc = $config['module_name_sc'];
    }

    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_' . $this->moduleNameSc . '_show_title'], 'value');

        $orderBy = $this->orderBy;

        $local = strtolower($this->session->getData('local'));

        $collection = $this->adapter->findBy(
            [],
            $orderBy($local)
        );

        return [
            'dtos' => $this->jsonSerializeObjectsById($collection)
        ];
    }
}
