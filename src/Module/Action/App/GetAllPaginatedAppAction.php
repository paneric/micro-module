<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\MicroModule\Interfaces\Action\GetAllPaginatedActionInterface;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetAllPaginatedAppAction extends Service implements GetAllPaginatedActionInterface
{
    protected $adapter;

    protected $moduleNameSc;
    protected $findByCriteria;
    protected $orderBy;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->findByCriteria = $config['find_by_criteria'];
        $this->orderBy = $config['order_by'];
        $this->moduleNameSc = $config['module_name_sc'];
    }

    public function getAllPaginated(string $page = null): array
    {
        $this->session->setFlash(['page_title' => 'content_' . $this->moduleNameSc . '_show_title'], 'value');

        $pagination = $this->session->getData('pagination');

        $findByCriteria = $this->findByCriteria;
        $orderBy = $this->orderBy;

        $local = strtolower($this->session->getData('local'));

        $collection = $this->adapter->findBy(
            $findByCriteria(),
            $orderBy($local),
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'dtos' => $this->jsonSerializeObjects($collection, false)//true if aggregates
        ];
    }
}
