<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\App;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\MicroModule\Interfaces\Action\GetOneByIdActionInterface;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetOneByIdAppAction extends Service implements GetOneByIdActionInterface
{
    protected $adapter;

    protected $findOneByCriteria;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->findOneByCriteria = $config['find_one_by_criteria'];
    }

    public function getOneById(string $id): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        $dto = $this->adapter->findOneBy($findOneByCriteria($id));

        if ($dto ===  null) {
            return null;
        }

        return [
            'dto' => $dto->convert()
        ];
    }
}
