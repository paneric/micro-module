<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\Api;

use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\MicroModule\Interfaces\Action\Api\GetOneByIdApiActionInterface;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;

class GetOneByIdApiAction extends Service implements GetOneByIdApiActionInterface
{
    protected $adapter;

    protected $findOneByCriteria;

    protected $status;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->findOneByCriteria = $config['find_one_by_criteria'];
    }

    public function getOneById(string $id): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        $dto = $this->adapter->findOneBy($findOneByCriteria($id));

        if ($dto ===  null) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Resource not found'
            ];
        }

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $dto->convert(),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
