<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\Api;

use Paneric\CSRTriad\Service;
use Paneric\MicroModule\Interfaces\Action\Api\AlterApiActionInterface;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class AlterApiAction extends Service implements AlterApiActionInterface
{
    protected $adapter;

    protected $daoClass;
    protected $dtoClass;
    protected $updateUniqueCriteria;
    protected $findOneByCriteria;

    protected $status;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->daoClass = $config['dao_class'];
        $this->dtoClass = $config['dto_class'];
        $this->updateUniqueCriteria = $config['update_unique_criteria'];
        $this->findOneByCriteria = $config['find_one_by_criteria'];
    }

    public function update(Request $request, string $id): ?array
    {
        if (!in_array('application/json;charset=utf-8', $request->getHeader('Content-Type'), true)) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'Invalid request type.'
            ];
        }

        $findOneByCriteria = $this->findOneByCriteria;

        $validationReport = $request->getAttribute('validation');

        if (empty($validationReport[$this->dtoClass])) {
            $attributes = $request->getParsedBody();

            $dao = new $this->daoClass();
            $dao->hydrate($attributes);

            $updateUniqueCriteria = $this->updateUniqueCriteria;

            if ($this->adapter->updateUnique(
                    $updateUniqueCriteria($attributes, $id),
                    $findOneByCriteria($id),
                    $dao
                ) !== null) {
                $this->status = 200;

                return [
                    'status' => $this->status,
                ];
            }

            $dto = new $this->dtoClass();
            $dto->hydrate($attributes);

            $this->status = 200;

            return [
                'status' => $this->status,
                'error' => 'db_update_unique_error',
                'body' => $dto->convert()
            ];
        }

        $dao = $this->adapter->findOneBy($findOneByCriteria($id));

        $dto = new $this->dtoClass();
        $dto->hydrate($dao->convert());

        $this->status = 200;

        return [
            'status' => $this->status,
            'error' => $validationReport,
            'body' => $dto->convert()
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
