<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\Api;

use Paneric\CSRTriad\Service;
use Paneric\MicroModule\Interfaces\Action\Api\DeleteApiActionInterface;
use Paneric\MicroModule\Interfaces\Repository\ModuleRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class DeleteApiAction extends Service implements DeleteApiActionInterface
{
    protected $adapter;

    private $findOneByCriteria;

    protected $status;

    public function __construct(ModuleRepositoryInterface $adapter, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->adapter = $adapter;

        $this->findOneByCriteria = $config['find_one_by_criteria'];
    }

    public function delete(Request $request, string $id): ?array
    {
        $findOneByCriteria = $this->findOneByCriteria;

        if ($this->adapter->delete($findOneByCriteria($id)) === 0) {
            $this->status = 400;

            return  [
                'status' => $this->status,
                'error' => 'Missing/invalid query parameter.'
            ];
        }

        $this->status = 200;

        return  [
            'status' => $this->status,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
