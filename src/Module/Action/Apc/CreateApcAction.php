<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\MicroModule\Interfaces\Action\CreateActionInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CreateApcAction extends Service implements CreateActionInterface
{
    protected $manager;
    private $config;

    public function __construct(HttpClientManager $manager, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function create(Request $request): ?array
    {
        $attributes = $request->getParsedBody();

        if ($request->getMethod() === 'POST') {
            $options = [
                'headers' => ['Content-Type' => 'application/json;charset=utf-8',],
                'json' => $attributes,
            ];

            $jsonResponse = $this->manager->getJsonResponse(
                'POST',
                sprintf(
                    '%s%s',
                    $this->config['base_url'],
                    $this->config['create_uri']
                ),
                $options
            );

            if (!isset($jsonResponse['error'])) {
                return null;
            }

            if ($jsonResponse['status'] === 200) {

                if (is_array($jsonResponse['error'])) {
                    $this->session->setFlash(
                        ['page_title' => 'form_' . $this->config['module_name_sc'] . '_add_title'],
                        'value'
                    );

                    return [
                        'dto' => $jsonResponse['body'],
                        'report' => $jsonResponse['error']
                    ];
                }

                $this->session->setFlash(
                    ['db_add_' . $this->config['module_name_sc'] . '_unique_error'],
                    'error'
                );

                return [
                    'dto' => $jsonResponse['body']
                ];
            }

            try {
                if ($jsonResponse['status'] === 400) {
                    throw new HttpBadRequestException(
                        $jsonResponse['error']
                    );
                }
            } catch (HttpBadRequestException $e) {
                error_log(sprintf(
                    "%s%s%s%s",
                    $e->getFile() . "\n",
                    $e->getLine() . "\n",
                    $e->getMessage() . "\n",
                    $e->getTraceAsString() . "\n"
                ), 0);
            }
        }

        $this->session->setFlash(
            ['page_title' => 'form_' . $this->config['module_name_sc'] . '_add_title'],
            'value'
        );

        return [
            'dto' => [],
        ];
    }
}
