<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\MicroModule\Interfaces\Action\GetOneByIdActionInterface;
use Paneric\Slim\Exception\HttpBadRequestException;

class GetOneByIdApcAction extends Service implements GetOneByIdActionInterface
{
    protected $manager;
    private $config;

    public function __construct(HttpClientManager $manager, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function getOneById(string $id): ?array
    {
        $options = [
            'headers' => ['Accept' => 'application/json;charset=utf-8',],
        ];

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s%s',
                $this->config['base_url'],
                $this->config['get_one_by_id_uri'],// /api/bc/
                $id
            ),
            $options
        );

        if ($jsonResponse['status'] === 200) {
            return [
                'dto' =>$jsonResponse['body']
            ];
        }

        try {
            if ($jsonResponse['status'] === 400) {
                throw new HttpBadRequestException(
                    $jsonResponse['error']
                );
            }
        } catch (HttpBadRequestException $e) {
            error_log(sprintf(
                "%s%s%s%s",
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $e->getMessage() . "\n",
                $e->getTraceAsString() . "\n"
            ), 0);
        }

        return null;
    }
}
