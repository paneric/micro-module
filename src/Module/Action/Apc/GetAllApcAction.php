<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\MicroModule\Interfaces\Action\GetAllActionInterface;

class GetAllApcAction extends Service implements GetAllActionInterface
{
    protected $manager;
    private $config;

    public function __construct(HttpClientManager $manager, SessionInterface $session, array $config)
    {
        parent::__construct($session);

        $this->manager = $manager;

        $this->config = $config;
    }

    public function getAll(): array
    {
        $this->session->setFlash(
            ['page_title' => 'content_' . $this->config['module_name_sc'] . '_show_title'],
            'value'
        );

        $options = [
            'headers' => ['Content-Type' => 'application/json;charset=utf-8',],
            'query' => [
                'local' => strtolower($this->session->getData('local')),
            ],
        ];

        $jsonResponse = $this->manager->getJsonResponse(
            'GET',
            sprintf(
                '%s%s',
                $this->config['base_url'],
                $this->config['get_all_uri']
            ),
            $options
        );

        if ($jsonResponse['status'] === 200) {
            return [
                'dtos' =>$jsonResponse['body']
            ];
        }

        return [];
    }
}
