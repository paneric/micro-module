<?php

declare(strict_types=1);

namespace Paneric\MicroModule\Module\Action\Apc;

use Paneric\CSRTriad\Service;
use Paneric\HttpClient\HttpClientManager;
use Paneric\MicroModule\Interfaces\Action\AlterActionInterface;

use Paneric\MicroModule\Interfaces\Action\GetOneByIdActionInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class AlterApcAction extends Service implements AlterActionInterface
{
    protected $manager;
    protected $getOneByIdAction;
    private $config;

    public function __construct(
        GetOneByIdActionInterface $getOneByIdAction,
        HttpClientManager $manager,
        SessionInterface $session,
        array $config
    ) {
        parent::__construct($session);

        $this->manager = $manager;

        $this->getOneByIdAction = $getOneByIdAction;

        $this->config = $config;
    }

    public function update(Request $request, string $id): ?array
    {
        if ($request->getMethod() === 'POST') {
            $attributes = $request->getParsedBody();

            $options = [
                'headers' => ['Content-Type' => 'application/json;charset=utf-8',],
                'json' => $attributes,
            ];

            $jsonResponse = $this->manager->getJsonResponse(
                'PUT',
                sprintf(
                    '%s%s%s',
                    $this->config['base_url'],
                    $this->config['alter_uri'],
                    $id
                ),
                $options
            );

            if (!isset($jsonResponse['error'])) {
                return null;
            }

            if ($jsonResponse['status'] === 200) {

                if (is_array($jsonResponse['error'])) {
                    $this->session->setFlash(
                        ['page_title' => 'form_' . $this->config['module_name_sc'] . '_edit_title'],
                        'value'
                    );

                    return [
                        'dto' => $jsonResponse['body'],
                        'report' => $jsonResponse['error']
                    ];
                }

                $this->session->setFlash(
                    ['db_update_' . $this->config['module_name_sc'] . '_unique_error'],
                    'error'
                );

                return [
                    'dto' => $jsonResponse['body']
                ];
            }

            try {
                if ($jsonResponse['status'] === 400) {
                    throw new HttpBadRequestException(
                        $jsonResponse['error']
                    );
                }
            } catch (HttpBadRequestException $e) {
                error_log(sprintf(
                    "%s%s%s%s",
                    $e->getFile() . "\n",
                    $e->getLine() . "\n",
                    $e->getMessage() . "\n",
                    $e->getTraceAsString() . "\n"
                ), 0);
            }
        }

        $this->session->setFlash(
            ['page_title' => 'form_' . $this->config['module_name_sc'] . '_edit_title'],
            'value'
        );

        return [
            'dto' => $this->getOneByIdAction->getOneById($id)['dto'],
        ];
    }
}
